Given('I am a teacher') do
    @user = FactoryBot.create :teacher 
    #pending # Write code here that turns the phrase above into concrete actions
end

Given(/^there is a project$/) do 
    @project = FactoryBot.create :project 
end 

Given(/^I want to add a student to the project$/) do 
    @student = FactoryBot.build :student 
end 
    
Given(/^I am signed in$/) do
    visit '/users/sign_in' 
    fill_in 'Email', with: @user.email 
    fill_in 'Password', with: @user.password 
    click_button 'Log in'
end
    
When(/^I visit the projects page$/) do 
    visit '/projects' 
end 
    
Then(/^I should see a link for the project$/) do 
    expect(page).to have_link('Show', href: project_path(@project)) 
end 

When(/^I click the link for the project$/) do 
    find_link('Show', href: project_path(@project)).click 
end 
    
Then(/^I should see the details of my project$/) do 
    save_and_open_page 
    expect(page).to have_content "Name: #{@project.name}" 
    expect(page).to have_content "Url: #{@project.url}" 
end 
  
Then(/^I should see a form to add a student$/) do 
    expect(page).to have_selector('form#new_student') 
end

When(/^I submit the form$/) do 
    find_button('Create Student').click
end 

And(/^I should see the student added to the project$/) do 
    # expect(page).to have_content(@student)
    @project.student.each do |student|
        puts "Checking that student #{student.inspect} is shown in the page."
        expect(page).to have_content(student.name)
        expect(page).to have_content(student.studentid)
    end
end 
  
#   Given('there is a project') do
#     pending # Write code here that turns the phrase above into concrete actions
#   end
  
#   Given('I want to add a student to the project') do
#     pending # Write code here that turns the phrase above into concrete actions
#   end
  
#   Given('I am signed in') do
#     pending # Write code here that turns the phrase above into concrete actions
#   end
  
#   When('I visit the projects page') do
#     pending # Write code here that turns the phrase above into concrete actions
#   end
  
#   Then('I should see a link for the project When I click the link for the project') do
#     pending # Write code here that turns the phrase above into concrete actions
#   end
  
#   Then('I should see the details of my project And I should see a form to add a student When I submit the form') do
#     pending # Write code here that turns the phrase above into concrete actions
#   end
  
#   Then('I should see the details of my project') do
#     pending # Write code here that turns the phrase above into concrete actions
#   end
  
#   Then('I should see the student added to the project') do
#     pending # Write code here that turns the phrase above into concrete actions
#   end
  