class Student < ApplicationRecord
    has_many :student_projects, dependent: :destroy
    has_many :projects, through: :student_projects

    validates_presence_of :name 
    validates_uniqueness_of :name 

    validates_presence_of :studentid 
    validates_uniqueness_of :studentid 


end
