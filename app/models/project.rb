class Project < ApplicationRecord
    has_many :student_projects, dependent: :destroy
    has_many :students, through: :student_projects

    validates_presence_of :name
    validates_uniqueness_of :name 
end
